// Lesson Proper
/*
	What is javascript- is a scripting languange that enables you to make interactive web pages
	Two ways to create comments:
	- // or ctrl + / - creates single comment
	- /* or ctrl + shift + / = creates a multiline comment */

console.log('hello, everyone!');
// this allows us to show/display data in our console. The console is a part of our browser with which we can use to display our data.
console.log(1+1);
console.log('Guillermo');
console.log('Spaghetti');
/*
	Statement and Syntax
	- are instructions/expression we add to our program which will then be communicated to our computers. In JS, it is not required.
	- Syntax in programming is a set of rules tht describes how statements or instructions are properly made or constructed
	ex. Line/Blocks of code follow a certain set of rules for it to work appropriately
*/

/*
	Variables - are containers of data
	to create a variable, we use let keyword, an assignment operator (=)
*/
let name = "Thonie";
console.log(name);

let num = '5'; // put qoutation so it becomes string
let num1 = 10;
console.log(num);
console.log(num1);
console.log(num + num1);
// concatenation is when you add a number and string

/*
	Types of creating Variable
	1. Declaration - create variable using let keyword
	2. Initialization - we assign the initial data/value using an assignment operator after the variable has been declared.
*/

let myVariable = "Initial Value";
console.log(myVariable);

/*
	Not defined vs undefined
	- not defined is an error
	- undefined means variable has been declared by there was no initial value
*/

let myVariable2;

console.log(myVariable2);

// declaring the variable now
myVariable2 = "New Value";
console.log(myVariable2);

let bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy);

// assigining new value to a variable (bestFinalFantasy)
// you can change the value of variable that has been declared
bestFinalFantasy = "Final Fantasy VII";
console.log(bestFinalFantasy);

bestStarWars = "Star Wars 8";
//this will work without let keyword, but advisable and a good practice. What will happen is that it will automatically create a variable but you will have no idea if this is a constant or variable.
console.log(bestStarWars);

// Constant is a container for data like a variable. However, constatnt values don not and should not be changed. You cannot create a constant without intiialization or initial value.

const myConstant = "constant value";
console.log(myConstant);
// don't change/update the value of a const.

const pi = 3.1416;
const boilingPointCelsius = 100;

// Mini Activity

let role = "Manager";
const tin = "12444-1230 ";
let name2 = "Juan Dela Cruz ";

role = "Director";
console.log(name2, role, tin);

/*
		Convetions in creating variables/constants
		- we use the let keyword for variables, use const keyword for constant
		- Variables and constants are named in small caps because there are other JS keywords that start in capital letters
		- don't name variables with spaces.
			- camelCase - is a convention of writing variables by adding the first word in smallcaps and the ff. words starting with capital letter but still no use of spaces
			ex. lastName, boilingPoint
		- variable names define the values they contain. Name your variables appropriately for the value thy contain.
*/

/*
	Data Types - is differentiated into different types and can do different things about this data. For most programming language, first we have to decare the type of data before we are able to create a variable and store data in it.
	- we use literals to create them. 
	- string use '' or ""
	- objects use literals = {}
	- arrays use array literals = []
*/

let state = "California";
let country1 = "U. S. A";
let state1 = "Florida";
let province1 = "Rizal";
let city = "Tokyo";
let city1 = "Copenhagen";
let country2 = "Japan";
let country3 = "Philippines";

// Mini Activity

let ph = province1 + ', ' + country3;
console.log(ph);
let us = state + ', ' + country1;
console.log(us);
let jp = city + ', ' + country2;
console.log(jp);

/*
	Numbers/Integers - integers are number data which can be use for mathematical operations.
*/

let numString1 = "5";
let numString2 = "6";
let number1 = 10;
let number2 = 6;

console.log(numString1 + numString2);
console.log(number1 + number2);

let number3 = 5.5;
let number4 = .5;

console.log(number1 + number4);
console.log(number3 + number4);

// Note: when a number/integer is added to a string, it results to concatenation
console.log(numString1 + number1);
console.log(number2 + numString2);

/*
	Boolean - used for logical operations or for if-else condition
	- the variable is usually a yes or no question
*/

let isAdmin = true;
let isMarried = false;
let isMVP = true;

console.log("is she marries? " + isMarried);
console.log("Is Stephen Curry an MVP? " +isMVP);
console.log("Is he the current admin? " +isAdmin);

/*
	Arrays - are special kind of data tupe used to store multiple values.
	- can store date with different type, however, this is not a good practice.
*/

let array1 = ["Goku", "Gohan", "Vegeta", "Trunks", "Broly"];
let array2 = ["One Punch Man", "Saitama", true, 5000];
console.log(array1);
console.log(array2);

/*
	Objects - are another special kind of date type used to mimic real world items/objects, use to create a complex data that contain pieces of information that relate to each other
	- each field in an object is called property. Properties are separated by a comma'
	- Each property is pair of key:value
	ex. hero =
			heroName: "One Punch Man",
			realName: "Saitama",
			powerlevel: 5000,
			isActive: true;
*/

let hero = {
			heroName: "One Punch Man",
			realName: "Saitama",
			powerlevel: 5000,
			isActive: true,
}

console.log(hero);

// Mini Activity

let bandmember = {
			firstName: "Michael",
			lastName: "Poulsen",
			isDeveloper: false,
			age: 46,
}

console.log(bandmember);



